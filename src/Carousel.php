<?php

namespace txd\widgets\carousel;

use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\JsExpression;
use yii\web\View;

/**
 * Class Carousel
 *
 * @author Tuxido <hello@tuxido.ro>
 * @link http://idangero.us/swiper/
 */
class Carousel extends Widget
{
	// Item config
	const ITEM_IMAGE = 'carousel-item-img';
	const ITEM_BACKGROUND = 'carousel-item-bg';

	// Caption type
	const CAPTION_TITLE = 'caption-title';
	const CAPTION_SUBTITLE = 'caption-subtitle';
	const CAPTION_TEXT = 'caption-text';
	const CAPTION_CTA = 'caption-cta';

	// Caption position
	const CAPTION_LEFT_TOP = 'text-left caption-top';
	const CAPTION_LEFT_CENTER = 'text-left caption-center';
	const CAPTION_LEFT_BOTTOM = 'text-left caption-bottom';
	const CAPTION_CENTER_TOP = 'text-center caption-top';
	const CAPTION_CENTER_CENTER = 'text-center caption-center';
	const CAPTION_CENTER_BOTTOM = 'text-center caption-bottom';
	const CAPTION_RIGHT_TOP = 'text-right caption-top';
	const CAPTION_RIGHT_CENTER = 'text-right caption-center';
	const CAPTION_RIGHT_BOTTOM = 'text-right caption-bottom';

	/**
	 * @var array The carousel items.
	 */
	public $items = [];

	/**
	 * @var array The widget options.
	 */
	public $options = [];

	/**
	 * @var bool Flag that indicates if the carousel has navigation.
	 */
	public $navigation = true;

	/**
	 * @var bool Flag that indicates if the carousel has pagination.
	 */
	public $pagination = true;

	/**
	 * @var bool Flag that indicates if the carousel has scrollbar.
	 */
	public $scrollbar = true;

	/**
	 * @var array The client (JS) options.
	 */
	public $clientOptions = [];

	/**
	 * @var array The client (JS) events.
	 */
	public $clientEvents = [];

	/**
	 * @var string The client (JS) selector.
	 */
	private $_clientSelector;

	/**
	 * @var string The global widget (JS) hash variable.
	 */
	private $_hashVar;

	/**
	 * @inheritdoc
	 */
	public function init()
	{
		parent::init();

		$this->setupProperties();
		$this->registerAssets();

		ob_start();
	}

	/**
	 * @inheritdoc
	 */
	public function run()
	{
		$slides = ob_get_clean();
		if (!empty($this->items)) {
			$slides = $this->render('slide');
		}

		$content = [];
		$content[] = Html::beginTag('div', $this->options);
		$content[] = Html::beginTag('div', ['class' => 'carousel-wrapper swiper-wrapper']);
		$content[] = $slides;
		$content[] = Html::endTag('div');

		if ($this->navigation === true) {
			$content[] = Html::beginTag('div', ['class' => 'carousel-navigation']);
			$content[] = Html::tag('div', null, ['class' => 'carousel-navigation-prev swiper-button-prev']);
			$content[] = Html::tag('div', null, ['class' => 'carousel-navigation-next swiper-button-next']);
			$content[] = Html::endTag('div');
		}
		if ($this->pagination === true) {
			$content[] = Html::tag('div', null, ['class' => 'carousel-pagination swiper-pagination']);
		}
		if ($this->scrollbar === true) {
			$content[] = Html::tag('div', null, ['class' => 'carousel-scrollbar swiper-scrollbar']);
		}

		$content[] = Html::endTag('div');

		return implode("\n", $content);
	}

	/**
	 * Gets the client selector.
	 *
	 * @return string
	 */
	public function getClientSelector()
	{
		if (!$this->_clientSelector) {
			$this->_clientSelector = '#' . $this->getId();
		}
		return $this->_clientSelector;
	}

	/**
	 * Gets the hash variable.
	 *
	 * @return string
	 */
	public function getHashVar()
	{
		if (!$this->_hashVar) {
			$this->_hashVar = 'carousel_' . hash('crc32', $this->buildClientOptions());
		}
		return $this->_hashVar;
	}

	/**
	 * Sets the widget properties.
	 */
	protected function setupProperties()
	{
		$this->options = array_merge([
			'id' => $this->getId(),
			'class' => 'swiper-container',
			'data' => [
				'carousel-options' => $this->getHashVar(),
			],
		], $this->options);

		Html::addCssClass($this->options, ['carousel-container', 'swiper-container']);
		$this->options['data']['carousel-options'] = $this->getHashVar();
		if ($this->options['id']) {
			$this->setId($this->options['id']);
		}
	}

	/**
	 * Builds the client options.
	 *
	 * @return string
	 */
	protected function buildClientOptions()
	{
		$selector = $this->getClientSelector();
		$defaultClientOptions = [];

		if ($this->navigation === true) {
			$defaultClientOptions['navigation'] = [
				'prevEl' => "$selector .swiper-button-prev",
				'nextEl' => "$selector .swiper-button-next",
			];
		}
		if ($this->pagination === true) {
			$defaultClientOptions['pagination'] = [
				'el' => "$selector .swiper-pagination",
			];
		}
		if ($this->scrollbar === true) {
			$defaultClientOptions['scrollbar'] = [
				'el' => "$selector .swiper-scrollbar",
			];
		}

		return Json::encode(array_merge_recursive($defaultClientOptions, $this->clientOptions));
	}

	/**
	 * Registers the widget assets.
	 */
	protected function registerAssets()
	{
		$view = $this->getView();

		// Register assets
		CarouselAsset::register($view);

		// Register widget hash JavaScript variable
		$view->registerJs("var {$this->getHashVar()} = {$this->buildClientOptions()};", View::POS_HEAD);

		// Build client script
		$js = "jQuery('{$this->getClientSelector()}').yiiCarousel({$this->getHashVar()})";

		// Build client events
		if (!empty($this->clientEvents)) {
			foreach ($this->clientEvents as $clientEvent => $eventHandler) {
				if (!($eventHandler instanceof JsExpression)) {
					$eventHandler = new JsExpression($eventHandler);
				}
				$js .= ".on('{$clientEvent}', {$eventHandler})";
			}
		}

		// Register widget JavaScript
		$view->registerJs("{$js};");
	}
}
