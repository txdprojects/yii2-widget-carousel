<?php

namespace txd\widgets\carousel;

use yii\web\AssetBundle;

class CarouselAsset extends AssetBundle
{
	/**
	 * @inheritdoc
	 */
	public $css = [
		'css/swiper.min.css',
		'css/yii.carousel.css',
	];

	/**
	 * @inheritdoc
	 */
	public $js = [
		'js/swiper.min.js',
		'js/yii.carousel.js',
	];

	/**
	 * @inheritdoc
	 */
	public $depends = [
		'\yii\web\JqueryAsset',
	];

	/**
	 * @inheritdoc
	 */
	public function init()
	{
		parent::init();

		$this->sourcePath = __DIR__ . '/assets';
	}
}
