<?php
/* @var $this yii\web\View */
/* @var $widget txd\widgets\carousel\Carousel */

use yii\helpers\Html;

$widget = $this->context;
?>

<?php foreach ($widget->items as $item) : ?>
	<?php
	$itemAttributes = [
		'class' => 'carousel-item swiper-slide',
	];
	Html::addCssClass($itemAttributes, ($item['type'] ?: $widget::ITEM_BACKGROUND));
	if (!$item['type'] || $item['type'] === $widget::ITEM_BACKGROUND) {
		Html::addCssStyle($itemAttributes, ['background-image' => 'url("' . $item['src'] . '")']);
	}
	?>

	<div <?= Html::renderTagAttributes($itemAttributes) ?>>

		<?php if ($item['type'] === $widget::ITEM_IMAGE): ?>
			<?= Html::img($item['src'], [
				'class' => 'img-responsive carousel-image',
				'alt' => !empty($item['captions']) ? Html::encode($item['captions'][0]['title']) : $item['src'],
			]) ?>
		<?php elseif (isset($item['content'])): ?>
			<?= $item['content'] ?>
		<?php endif; ?>

		<?php if (is_array($item['captions']) && !empty($item['captions'])) : ?>
			<div class="container-fluid carousel-caption">
				<div class="caption-cell <?= $item['position'] ?>">
					<?php foreach ($item['captions'] as $caption) : ?>
						<?php
						$captionAttributes = [
							'class' => 'caption-item',
						];
						Html::addCssClass($captionAttributes, $caption['type']);
						if ($caption['animation']) {
							$captionAttributes['data-animation'] = $caption['animation'];
						}
						?>
						<div <?= Html::renderTagAttributes($captionAttributes) ?>><?= $caption['content'] ?></div>
					<?php endforeach; ?>
				</div>
			</div>
		<?php endif; ?>

	</div>
<?php endforeach; ?>
