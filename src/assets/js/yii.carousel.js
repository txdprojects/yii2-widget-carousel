(function ($, window, document, undefined) {
	/**
	 * Constants
	 * @constant {String} PLUGIN_NAME
	 * @constant {String} PLUGIN_VERSION
	 * @constant {String} DATA_KEY
	 * @constant {Object} DEFAULTS
	 */
	var PLUGIN_NAME = 'yiiCarousel',
		PLUGIN_VERSION = '1.0.0',
		EVENT_NS = '.' + PLUGIN_NAME,
		DATA_KEY = 'plugin_' + PLUGIN_NAME,
		DEFAULTS = {
			init: false,

			onInit: function () {},
			onDestroy: function () {}
		};

	/**
	 * Plugin
	 *
	 * @param element
	 * @param options
	 * @param metadata
	 * @constructor
	 */
	var Plugin = function (element, options, metadata) {
		if (!element) {
			console.error('[' + PLUGIN_NAME + ']: DOM element is missing');
			return;
		}
		this.element = element;
		this.options = $.extend({}, DEFAULTS, options, metadata);
		this.init();
	};

	/**
	 * Initialization
	 */
	Plugin.prototype.init = function () {
		this._cacheElements();
		this.carousel = new Swiper(this.element, this.options);
		this._bindEvents();
		this.carousel.init();
		this._hook('onInit');
	};

	/**
	 * Caches DOM Elements.
	 *
	 * @private
	 */
	Plugin.prototype._cacheElements = function () {
		this.$window = $(window);
		this.$document = $(document);
		this.$html = $('html');
		this.$body = this.$html.children('body');
		this.$element = $(this.element);
	};

	/**
	 * Binds Events.
	 *
	 * @private
	 */
	Plugin.prototype._bindEvents = function () {
		this.carousel.on('init', this._onInit.bind(this));
		this.carousel.on('slideChange', this._onSlideChange.bind(this));
		this.$document.on('animationend', '.carousel-caption', this._onCaptionAnimationEnd.bind(this));
	};

	/**
	 * Handles Swiper init event.
	 *
	 * @private
	 */
	Plugin.prototype._onInit = function () {
		this.animateCaptions();
	};

	/**
	 * Handles Swiper slideChange event.
	 *
	 * @private
	 */
	Plugin.prototype._onSlideChange = function () {
		this.animateCaptions();
	};

	/**
	 * Handles caption animationend event.
	 *
	 * @param e
	 * @private
	 */
	Plugin.prototype._onCaptionAnimationEnd = function (e) {
		var $caption = $(e.target);
		// Remove animation
		$caption.addClass('animated').css('animation', '');
	};

	/**
	 * Display carousel captions.
	 */
	Plugin.prototype.animateCaptions = function () {
		var $currentSlide = this.carousel.slides.eq(this.carousel.realIndex),
			$captionItems = $currentSlide.find('.caption-item');
		// Exit if there are no caption items
		if (!$captionItems.length) {
			return;
		}
		// Loop trough caption items
		$captionItems.each(function (index, caption) {
			var $caption = $(caption),
				captionData = $caption.data();
			// Apply rules from dataset
			$caption
				.removeClass('animated')
				.css({
					'animation': captionData.animation
				});
		});
	};

	/**
	 * Hooks callbacks.
	 *
	 * @access private
	 * @param [arguments]
	 */
	Plugin.prototype._hook = function () {
		var args = Array.prototype.slice.call(arguments),
			hookName = args.shift(),
			eventName = '';

		if (hookName.substr(0, 2) === 'on') {
			eventName = hookName.slice(2).charAt(0).toLowerCase() + hookName.slice(3);
		} else {
			eventName = hookName.charAt(0).toLowerCase() + hookName.slice(1);
		}
		eventName += EVENT_NS;

		// Execute the callback
		if (typeof this.options[hookName] === 'function') {
			this.options[hookName].apply(this.element, args);
		}

		// Trigger the event
		var event = $.Event(eventName, {
			target: this.element
		});
		this.$element.trigger(event, args);
	};

	/**
	 * Gets or sets a property.
	 *
	 * @access public
	 * @param {String} key
	 * @param {String} val
	 */
	Plugin.prototype.option = function (key, val) {
		if (val) {
			this.options[key] = val;
		} else {
			return this.options[key];
		}
	};

	/**
	 * Destroys the plugin instance.
	 *
	 * @public
	 */
	Plugin.prototype.destroy = function () {
		this._hook('onDestroy');
		this.$window.off(EVENT_NS);
		this.$document.off(EVENT_NS);
		this.$element.off(EVENT_NS);
		this.$element.removeData(DATA_KEY);
	};

	/**
	 * Plugin definition
	 * @function external "jQuery.fn".yiiCarousel
	 */
	$.fn[PLUGIN_NAME] = function (options) {
		var args = arguments;

		if (!options || typeof options === "object") {
			return this.each(function () {
				if (!$.data(this, DATA_KEY)) {
					var metadata = $(this).data();
					$.data(this, DATA_KEY, new Plugin(this, options, metadata));
				}
			});
		} else if (typeof args[0] === 'string') {
			var methodName = args[0].replace('_', ''),
				returnVal;

			this.each(function () {
				var instance = $.data(this, DATA_KEY);

				if (instance && typeof instance[methodName] === 'function') {
					returnVal = instance[methodName].apply(instance, Array.prototype.slice.call(args, 1));
				} else {
					throw new Error('Could not call method "' + methodName + '" on jQuery.fn.' + PLUGIN_NAME);
				}
			});

			return (typeof returnVal !== 'undefined') ? returnVal : this;
		}
	};

	/**
	 * Expose global
	 */
	this[PLUGIN_NAME] = Plugin;

})(jQuery, window, document);
